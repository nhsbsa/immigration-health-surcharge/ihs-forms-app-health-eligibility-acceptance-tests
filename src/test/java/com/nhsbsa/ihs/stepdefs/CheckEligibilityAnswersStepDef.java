package com.nhsbsa.ihs.stepdefs;

import com.nhsbsa.ihs.driver.Config;
import com.nhsbsa.ihs.pageobjects.*;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.junit.Assert;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.JavascriptExecutor;
import java.io.IOException;

import static com.nhsbsa.ihs.stepdefs.Constants.*;

public class CheckEligibilityAnswersStepDef {

    private WebDriver driver;
    private CommonPage commonPage;
    private IHSPaidPage IHSPaidPage;
    private VisaTypePage visaTypePage;
    private QualifiedHealthProfessionalPage qualifiedHealthProfessionalPage;
    private HealthSocialCarePage healthSocialCarePage;
    private WorkSixMonthsPage workSixMonthsPage;
    private WorkAverageHoursPage workAverageHoursPage;
    private CheckEligibilityAnswersPage checkEligibilityAnswersPage;
    private Page page;

    public CheckEligibilityAnswersStepDef() throws IOException {
        driver = Config.getDriver();
        commonPage = new CommonPage(driver);
        IHSPaidPage = new IHSPaidPage(driver);
        visaTypePage = new VisaTypePage(driver);
        qualifiedHealthProfessionalPage = new QualifiedHealthProfessionalPage(driver);
        healthSocialCarePage = new HealthSocialCarePage(driver);
        workSixMonthsPage = new WorkSixMonthsPage(driver);
        workAverageHoursPage = new WorkAverageHoursPage(driver);
        checkEligibilityAnswersPage = new CheckEligibilityAnswersPage(driver);
        page = new Page(driver);
    }

    @When("^I hold (.*) visa and eligible for reimbursement$")
    public void iHoldVisaTypeVisaAndEligibleForReimbursement(String visaType) {
        switch (visaType) {
            case "Tier2":
                IHSPaidPage.selectYesRadioButtonAndClickContinue();
                visaTypePage.selectTier2AndContinue();
                qualifiedHealthProfessionalPage.selectNoRadioButtonAndClickContinue();
                healthSocialCarePage.selectYesRadioButtonAndClickContinue();
                workSixMonthsPage.selectYesRadioButtonAndClickContinue();
                workAverageHoursPage.selectYesRadioButtonAndClickContinue();
                break;
            case "Other":
                IHSPaidPage.selectYesRadioButtonAndClickContinue();
                visaTypePage.selectOtherVisaAndContinue();
                healthSocialCarePage.selectYesRadioButtonAndClickContinue();
                workSixMonthsPage.selectYesRadioButtonAndClickContinue();
                workAverageHoursPage.selectYesRadioButtonAndClickContinue();
                break;
        }
    }

    @And("^I select (.*) on Check Eligibility Answers page$")
    public void iSelectHyperlinkOnCheckEligibilityAnswersPage(String hyperlink) {
        switch (hyperlink) {
            case "Service Name link":
                commonPage.serviceNameLink();
                break;
            case "GOV.UK link":
                commonPage.govUKLink();
                break;
            case "Change IHS Paid link":
                checkEligibilityAnswersPage.changeLinkIHSPayment();
                break;
            case "Change Visa Type link":
                checkEligibilityAnswersPage.changeLinkTier2Question();
                break;
            case "Change Health Professional link":
                checkEligibilityAnswersPage.changeLinkQualifiedHealthProfessional();
                break;
            case "Change Health Social Care link":
                checkEligibilityAnswersPage.changeLinkHSCWork();
                break;
            case "Change HSC 6 Months link":
                checkEligibilityAnswersPage.changeLinkSixMonths();
                break;
            case "Change Average 16 Hours link":
                checkEligibilityAnswersPage.changeLinkAverageTime();
                break;
        }
    }

    @And("^I have verified my eligibility answers$")
    public void iHaveVerifiedMyEligibilityAnswers() {
        checkEligibilityAnswersPage.checkAndAcceptButton();
    }

    @Then("^I will see (.*) eligibility answers$")
    public void iWillSeeAnswersEligibilityAnswers(String answers) {
        switch (answers) {
            case "1=Yes 2=Yes 3=Yes 4=Yes and 5=Yes":
                page.waitForPageLoad();
                Assert.assertEquals("Yes", checkEligibilityAnswersPage.getIHSPaidAnswer().trim());
                Assert.assertEquals("Yes", checkEligibilityAnswersPage.getVisaTypeAnswer().trim());
                Assert.assertEquals("Yes", checkEligibilityAnswersPage.getHealthSocialCareAnswer().trim());
                Assert.assertEquals("Yes", checkEligibilityAnswersPage.getWorkSixMonthsAnswer().trim());
                Assert.assertEquals("Yes", checkEligibilityAnswersPage.getAverageSixteenHoursAnswer().trim());
                break;
            case "1=Yes 2=Yes 3=No 4=Yes 5=Yes and 6=Yes":
                page.waitForPageLoad();
                Assert.assertEquals("Yes", checkEligibilityAnswersPage.getIHSPaidAnswer().trim());
                Assert.assertEquals("Yes", checkEligibilityAnswersPage.getVisaTypeAnswer().trim());
                Assert.assertEquals("No", checkEligibilityAnswersPage.getQualifiedHealthProfessionalAnswer().trim());
                Assert.assertEquals("Yes", checkEligibilityAnswersPage.getHealthSocialCareAnswer().trim());
                Assert.assertEquals("Yes", checkEligibilityAnswersPage.getWorkSixMonthsAnswer().trim());
                Assert.assertEquals("Yes", checkEligibilityAnswersPage.getAverageSixteenHoursAnswer().trim());
                break;
        }
    }

    @And("^I submit the eligibility answers$")
    public void iSubmitTheEligibilityAnswersAnswers() {
        checkEligibilityAnswersPage.checkAndAcceptButton();
    }

    @When("^I continue without changing my answer$")
    public void iContinueWithoutChangingMyAnswer() {
        commonPage.clickContinueButton();
    }

    @And("^I will see (.*) selected$")
    public void iWillSeeAnswerSelected(String answer) {
        switch (answer) {
            case "Paid IHS Yes":
                Assert.assertTrue(IHSPaidPage.isYesRadioButtonSelected());
                break;
            case "Tier2 Visa Yes":
                Assert.assertTrue(visaTypePage.isTier2VisaSelected());
                break;
            case "Other Visa Yes":
                Assert.assertTrue(visaTypePage.isOtherVisaSelected());
                break;
            case "Health Professional No":
                Assert.assertTrue(qualifiedHealthProfessionalPage.isNoRadioButtonSelected());
                break;
            case "Health Social Care Yes":
                Assert.assertTrue(healthSocialCarePage.isYesRadioButtonSelected());
                break;
            case "Work 6 Months Yes":
                Assert.assertTrue(workSixMonthsPage.isYesRadioButtonSelected());
                break;
            case "Average 16 Hours Yes":
                Assert.assertTrue(workAverageHoursPage.isYesRadioButtonSelected());
                break;
        }
    }

    @And("^My answer remains the same as (.*)$")
    public void myAnswerRemainsTheSameAsAnswer(String answer) {
        switch (answer) {
            case "Paid IHS Yes":
                Assert.assertEquals("Yes", checkEligibilityAnswersPage.getIHSPaidAnswer());
                break;
            case "Tier2 Visa Yes":
                Assert.assertEquals("Yes", checkEligibilityAnswersPage.getVisaTypeAnswer());
                break;
            case "Health Professional No":
                Assert.assertEquals("No", checkEligibilityAnswersPage.getQualifiedHealthProfessionalAnswer());
                break;
            case "Health Social Care Yes":
                Assert.assertEquals("Yes", checkEligibilityAnswersPage.getHealthSocialCareAnswer());
                break;
            case "Work 6 Months Yes":
                Assert.assertEquals("Yes", checkEligibilityAnswersPage.getWorkSixMonthsAnswer());
                break;
            case "Average 16 Hours Yes":
                Assert.assertEquals("Yes", checkEligibilityAnswersPage.getAverageSixteenHoursAnswer());
                break;
        }
    }

    @When("^I change my answer as (.*)$")
    public void iChangeMyAnswerAsRadioButton(String radioButton) {
        switch (radioButton) {
            case "Paid IHS No":
                IHSPaidPage.selectNoRadioButtonAndClickContinue();
                break;
            case "Tier5 Visa Yes":
                visaTypePage.selectTier5AndContinue();
                break;
            case "Tier2 Visa Yes":
                visaTypePage.selectTier2AndContinue();
                break;
            case "Other Visa Yes":
                visaTypePage.selectOtherVisaAndContinue();
                break;
            case "Health Professional Yes":
                qualifiedHealthProfessionalPage.selectYesRadioButtonAndClickContinue();
                break;
            case "Health Social Care No":
                healthSocialCarePage.selectNoRadioButtonAndClickContinue();
                break;
            case "Work 6 Months No":
                workSixMonthsPage.selectNoRadioButtonAndClickContinue();
                break;
            case "Average 16 Hours No":
                workAverageHoursPage.selectNoRadioButtonAndClickContinue();
                break;
        }
    }
}
