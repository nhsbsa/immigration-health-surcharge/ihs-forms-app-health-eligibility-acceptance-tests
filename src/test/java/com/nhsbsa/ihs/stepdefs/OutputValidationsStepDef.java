package com.nhsbsa.ihs.stepdefs;

import com.nhsbsa.ihs.driver.Config;
import com.nhsbsa.ihs.pageobjects.*;
import io.cucumber.java.en.Then;
import org.junit.Assert;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;

import java.io.IOException;

import static com.nhsbsa.ihs.stepdefs.Constants.*;
import static org.hamcrest.CoreMatchers.is;

public class OutputValidationsStepDef {

    private WebDriver driver;
    private CommonPage commonPage;
    private com.nhsbsa.ihs.pageobjects.IHSPaidPage IHSPaidPage;
    private VisaTypePage visaTypePage;
    private QualifiedHealthProfessionalPage qualifiedHealthProfessionalPage;
    private HealthSocialCarePage healthSocialCarePage;
    private WorkSixMonthsPage workSixMonthsPage;
    private WorkAverageHoursPage workAverageHoursPage;
    private NotEligiblePage notEligiblePage;
    private Page page;

    public OutputValidationsStepDef() throws IOException {
        driver = Config.getDriver();
        commonPage = new CommonPage(driver);
        IHSPaidPage = new IHSPaidPage(driver);
        visaTypePage = new VisaTypePage(driver);
        qualifiedHealthProfessionalPage = new QualifiedHealthProfessionalPage(driver);
        healthSocialCarePage = new HealthSocialCarePage(driver);
        workSixMonthsPage = new WorkSixMonthsPage(driver);
        workAverageHoursPage = new WorkAverageHoursPage(driver);
        notEligiblePage = new NotEligiblePage(driver);
        page = new Page(driver);
    }

    @Then("^I will see the (.*)$")
    public void iWillSeeTheOutput(String output) {
        switch (output) {
            case "Start screen":
                Assert.assertEquals(START_PAGE_TITLE, commonPage.getCurrentPageTitle());
                Assert.assertTrue(commonPage.getCurrentURL().contains( START_PAGE_URL));
                break;
            case "IHS Paid screen":
                Assert.assertEquals(IHS_PAID_PAGE_TITLE, commonPage.getCurrentPageTitle());
                Assert.assertTrue(commonPage.getCurrentURL().contains(IHS_PAID_PAGE_URL));
                break;
            case "Visa Type Question screen":
                Assert.assertEquals(VISA_TYPE_PAGE_TITLE, commonPage.getCurrentPageTitle());
                Assert.assertTrue(commonPage.getCurrentURL().contains(VISA_TYPE_PAGE_URL));
                break;
            case "Health Professional screen":
                Assert.assertEquals(QUALIFIED_HEALTH_PROFESSIONAL_PAGE_TITLE, commonPage.getCurrentPageTitle());
                Assert.assertTrue(commonPage.getCurrentURL().contains(QUALIFIED_HEALTH_PROFESSIONAL_PAGE_URL));
                break;
            case "Health Social Care screen":
                Assert.assertEquals(HEALTH_SOCIAL_CARE_PAGE_TITLE, commonPage.getCurrentPageTitle());
                Assert.assertTrue(commonPage.getCurrentURL().contains(HEALTH_SOCIAL_CARE_PAGE_URL));
                break;
            case "HSC 6 Months screen":
                Assert.assertEquals(WORK_SIX_MONTHS_PAGE_TITLE, commonPage.getCurrentPageTitle());
                Assert.assertTrue(commonPage.getCurrentURL().contains(WORK_SIX_MONTHS_PAGE_URL));
                break;
            case "Average 16 Hours screen":
                Assert.assertEquals(WORK_AVERAGE_HOURS_PAGE_TITLE, commonPage.getCurrentPageTitle());
                Assert.assertTrue(commonPage.getCurrentURL().contains(WORK_AVERAGE_HOURS_PAGE_URL));
                break;
            case "Check Eligibility Answers screen":
                Assert.assertEquals(CHECK_ELIGIBILITY_ANSWERS_PAGE_TITLE, commonPage.getCurrentPageTitle());
                Assert.assertTrue(commonPage.getCurrentURL().contains(CHECK_ELIGIBILITY_ANSWERS_PAGE_URL));
                break;
            case "Name screen":
                Assert.assertEquals(CLAIM_NAME_PAGE_TITLE, commonPage.getCurrentPageTitle());
                Assert.assertTrue(commonPage.getCurrentURL().contains(CLAIM_NAME_PAGE_URL));
                break;
            case "Not Eligible for Reimbursement or Refund screen":
                Assert.assertEquals(NOT_ELIGIBLE_IHS_NOT_PAID_PAGE_TITLE, commonPage.getCurrentPageTitle());
                Assert.assertTrue(commonPage.getCurrentURL().contains(NOT_ELIGIBLE_IHS_NOT_PAID_PAGE_URL));
                break;
            case "Tier5 Not Eligible to Apply screen":
                Assert.assertEquals(NOT_ELIGIBLE_TIER5_VISA_PAGE_TITLE, commonPage.getCurrentPageTitle());
                Assert.assertTrue(commonPage.getCurrentURL().contains(NOT_ELIGIBLE_TIER5_VISA_PAGE_URL));
                break;
            case "Not Eligible for Reimbursement but Refund screen":
                Assert.assertEquals(NOT_ELIGIBLE_TIER2_VISA_PAGE_TITLE, commonPage.getCurrentPageTitle());
                Assert.assertTrue(commonPage.getCurrentURL().contains(NOT_ELIGIBLE_TIER2_VISA_PAGE_URL));
                break;
            case "Not Eligible for Reimbursement screen":
                Assert.assertEquals(NOT_ELIGIBLE_JOB_RELATED_PAGE_TITLE, commonPage.getCurrentPageTitle());
                Assert.assertTrue(commonPage.getCurrentURL().contains(NOT_ELIGIBLE_JOB_RELATED_PAGE_URL));
                break;
            case "GOV UK screen":
                Assert.assertEquals(GOV_UK_PAGE, commonPage.getCurrentPageTitle());
                break;
            case "Help screen":
                Assert.assertEquals(HELP_PAGE, commonPage.getFooterLinksPageTitle());
                break;
            case "Pay for UK healthcare screen":
                Assert.assertEquals(PAY_FOR_UK_HEALTHCARE_PAGE_TITLE, commonPage.getFooterLinksPageTitle());
                notEligiblePage.navigateToMoreInfo();
                Assert.assertTrue(commonPage.getFooterLinksPageURL().equals(PAY_FOR_UK_HEALTHCARE_PAGE_URL));
                break;
            case "GOV UK Tier2 screen":
                Assert.assertEquals(GOV_UK_TIER2_PAGE_TITLE, commonPage.getFooterLinksPageTitle());
                notEligiblePage.navigateToTier2MoreInfo();
                Assert.assertTrue(commonPage.getFooterLinksPageURL().equals(GOV_UK_TIER2_PAGE_URL));
                break;
            case "AOMRC surcharge refund screen":
                Assert.assertEquals(AOMRC_REFUND_PAGE_TITLE, commonPage.getFooterLinksPageTitle());
                notEligiblePage.navigateToTier5VisaInfo();
                Assert.assertTrue(commonPage.getFooterLinksPageURL().equals(AOMRC_REFUND_PAGE_URL));
                break;
            case "Open Government Licence screen":
                Assert.assertEquals(OPEN_GOVERNMENT_LICENCE_PAGE, commonPage.getFooterLinksPageTitle());
                break;
            case "Crown Copyright screen":
                Assert.assertEquals(CROWN_COPYRIGHT_PAGE, commonPage.getFooterLinksPageTitle());
                break;
            case "Snap survey preview":
                Assert.assertEquals(SNAP_SURVEY_PREVIEW_PAGE, commonPage.getSurveyPreviewPageTitle());
                break;
            case "IHS Health Service Survey screen":
                Assert.assertEquals(HEALTH_SNAP_SURVEY_PAGE, commonPage.getFooterLinksPageTitle());
                break;
            case "Contact Us screen":
                Assert.assertEquals(CONTACT_US_PAGE_TITLE, commonPage.getFooterLinksPageTitle());
                commonPage.navigateToContactUs();
                Assert.assertTrue(commonPage.getFooterLinksPageURL().contains(CONTACT_US_PAGE_URL));
                break;
            case "Cookies Policy screen":
                Assert.assertEquals(COOKIES_POLICY_PAGE_TITLE, commonPage.getFooterLinksPageTitle());
                commonPage.navigateToCookies();
                Assert.assertTrue(commonPage.getFooterLinksPageURL().contains(COOKIES_POLICY_PAGE_URL));
                break;
            case "Accessibility Statement screen":
                Assert.assertEquals(ACCESSIBILITY_STATEMENT_PAGE_TITLE, commonPage.getFooterLinksPageTitle());
                commonPage.navigateToAccessibilityStatement();
                Assert.assertTrue(commonPage.getFooterLinksPageURL().contains(ACCESSIBILITY_STATEMENT_PAGE_URL));
                break;
            case "Terms and Conditions screen":
                Assert.assertEquals(TERMS_CONDITIONS_PAGE_TITLE, commonPage.getFooterLinksPageTitle());
                commonPage.navigateToTermsConditions();
                Assert.assertTrue(commonPage.getFooterLinksPageURL().contains(TERMS_CONDITIONS_PAGE_URL));
                break;
            case "Privacy Notice screen":
                Assert.assertEquals(PRIVACY_NOTICE_PAGE_TITLE, commonPage.getFooterLinksPageTitle());
                commonPage.navigateToPrivacyNotice();
                Assert.assertTrue(commonPage.getFooterLinksPageURL().contains(PRIVACY_NOTICE_PAGE_URL));
                break;
            case "Cookies Policy page":
                Assert.assertEquals(COOKIES_POLICY_PAGE_TITLE, commonPage.getFooterLinksPageTitle());
                break;
            case "Cookies accepted confirmation":
                Assert.assertTrue(commonPage.getCookiesAcceptedMessage().contains("You’ve accepted analytics cookies. You can"));
                break;
            case "Cookies rejected confirmation":
                Assert.assertTrue(commonPage.getCookiesRejectedMessage().contains("You’ve rejected analytics cookies. You can"));
                break;
            case "IHS Payment Error Message":
                Assert.assertEquals("Select 'Yes' if an immigration health surcharge has been paid on or after 31 March 2020", IHSPaidPage.getErrorMessage());
                break;
            case "Visa Type Error Message":
                Assert.assertEquals("Select the type of visa that the applicant holds", visaTypePage.getErrorMessage());
                break;
            case "Health Professional Error Message":
                Assert.assertEquals("Select 'Yes' if the applicant is a qualified health professional", qualifiedHealthProfessionalPage.getErrorMessage());
                break;
            case "Health Social Care Error Message":
                Assert.assertEquals("Select 'Yes' if applicant worked for a health or social care employer", healthSocialCarePage.getErrorMessage());
                break;
            case "HSC 6 Months Error Message":
                Assert.assertEquals("Select 'Yes' if applicant worked in the same job for 6 months or more", workSixMonthsPage.getErrorMessage());
                break;
            case "Average 16 Hours Error Message":
                Assert.assertEquals("Select 'Yes' if applicant worked an average of 16 hours per week", workAverageHoursPage.getErrorMessage());
                break;
            case "Claim Name screen":
                try{
                    page.waitForPageLoad();
                    Assert.assertTrue(commonPage.getCurrentURL().contains(CLAIM_NAME_PAGE_URL));
                    ((JavascriptExecutor)driver).executeScript("sauce:job-result=passed");
                }
                catch(AssertionError e){
                    Assert.assertThat(e.getMessage(),is("My Assertion error"));
                    ((JavascriptExecutor)driver).executeScript("sauce:job-result=failed");
                }
                break;
        }
    }
}
