package com.nhsbsa.ihs.stepdefs;

import com.nhsbsa.ihs.driver.Config;
import com.nhsbsa.ihs.pageobjects.*;
import io.cucumber.java.en.When;
import org.openqa.selenium.WebDriver;

public class EligibilityQuestionsStepDef {

    private WebDriver driver;
    private IHSPaidPage IHSPaidPage;
    private VisaTypePage visaTypePage;
    private QualifiedHealthProfessionalPage qualifiedHealthProfessionalPage;
    private HealthSocialCarePage healthSocialCarePage;
    private WorkSixMonthsPage workSixMonthsPage;
    private WorkAverageHoursPage workAverageHoursPage;

    public EligibilityQuestionsStepDef() {
        driver = Config.getDriver();
        IHSPaidPage = new IHSPaidPage(driver);
        visaTypePage = new VisaTypePage(driver);
        qualifiedHealthProfessionalPage = new QualifiedHealthProfessionalPage(driver);
        healthSocialCarePage = new HealthSocialCarePage(driver);
        workSixMonthsPage = new WorkSixMonthsPage(driver);
        workAverageHoursPage = new WorkAverageHoursPage(driver);
    }

    @When("^I (.*) paid the Immigration Health Surcharge$")
    public void iOptionPaidTheImmigrationHealthSurcharge(String ihsOption) {
        switch (ihsOption) {
            case "have":
                IHSPaidPage.selectYesRadioButtonAndClickContinue();
                break;
            case "have not":
                IHSPaidPage.selectNoRadioButtonAndClickContinue();
                break;
            case "":
                IHSPaidPage.continueButton();
                break;
        }
    }

    @When("^I hold (.*) visa$")
    public void iHoldVisaTypeVisa(String visaType) {
        switch (visaType) {
            case "Tier2":
                visaTypePage.selectTier2AndContinue();
                break;
            case "Tier5":
                visaTypePage.selectTier5AndContinue();
                break;
            case "Other":
                visaTypePage.selectOtherVisaAndContinue();
                break;
            case "":
                visaTypePage.continueButton();
                break;
        }
    }

    @When("^I (.*) a qualified health professional$")
    public void iOptionAQualifiedHealthProfessional(String qhpOption) {
        switch (qhpOption) {
            case "am":
                qualifiedHealthProfessionalPage.selectYesRadioButtonAndClickContinue();
                break;
            case "am not":
                qualifiedHealthProfessionalPage.selectNoRadioButtonAndClickContinue();
                break;
            case "":
                qualifiedHealthProfessionalPage.continueButton();
                break;
        }
    }

    @When("^I (.*) worked in health and social care$")
    public void iOptionWorkedInHealthAndSocialCare(String hscOption) {
        switch (hscOption) {
            case "have":
                healthSocialCarePage.selectYesRadioButtonAndClickContinue();
                break;
            case "have not":
                healthSocialCarePage.selectNoRadioButtonAndClickContinue();
                break;
            case "":
                healthSocialCarePage.continueButton();
                break;
        }
    }

    @When("^I (.*) worked for six months or more$")
    public void iOptionWorkedForSixMonthsOrMore(String wsmOption) {
        switch (wsmOption) {
            case "have":
                workSixMonthsPage.selectYesRadioButtonAndClickContinue();
                break;
            case "have not":
                workSixMonthsPage.selectNoRadioButtonAndClickContinue();
                break;
            case "":
                workSixMonthsPage.continueButton();
                break;
        }
    }

    @When("^I (.*) worked for an average of sixteen hours$")
    public void iOptionWorkedForAnAverageOfSixteenHours(String ashOption) {
        switch (ashOption) {
            case "have":
                workAverageHoursPage.selectYesRadioButtonAndClickContinue();
                break;
            case "have not":
                workAverageHoursPage.selectNoRadioButtonAndClickContinue();
                break;
            case "":
                workAverageHoursPage.continueButton();
                break;
        }
    }
}
