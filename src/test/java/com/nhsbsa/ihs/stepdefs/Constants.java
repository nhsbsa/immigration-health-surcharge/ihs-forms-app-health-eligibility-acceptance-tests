package com.nhsbsa.ihs.stepdefs;

public class Constants {

    public static final String START_PAGE_URL = "/apply-immigration-health-surcharge-refund/healthcare-setting";
    public static final String START_PAGE_TITLE = "Get an immigration health surcharge refund if you work in health and care: If you do not have a Tier 2 visa or you work in another health or care role - GOV.UK";

    public static final String IHS_PAID_PAGE_URL = "/health/eligibility/immigration-health-surcharge-paid";
    public static final String VISA_TYPE_PAGE_URL = "/health/eligibility/applicant-visa-type";
    public static final String QUALIFIED_HEALTH_PROFESSIONAL_PAGE_URL = "/health/eligibility/qualified-health-professional";
    public static final String HEALTH_SOCIAL_CARE_PAGE_URL = "/health/eligibility/health-social-care-job";
    public static final String WORK_SIX_MONTHS_PAGE_URL = "/health/eligibility/job-6-months";
    public static final String WORK_AVERAGE_HOURS_PAGE_URL = "/health/eligibility/job-average-16-hours";
    public static final String CHECK_ELIGIBILITY_ANSWERS_PAGE_URL = "/health/eligibility/check-eligibility-answers";
    public static final String CLAIM_NAME_PAGE_URL = "/health/claim/name";
    public static final String COOKIES_POLICY_PAGE_URL = "/health/help/cookies";
    public static final String ACCESSIBILITY_STATEMENT_PAGE_URL = "/health/help/accessibility-statement";
    public static final String CONTACT_US_PAGE_URL = "/health/help/contact";
    public static final String TERMS_CONDITIONS_PAGE_URL = "/health/help/terms-conditions";
    public static final String PRIVACY_NOTICE_PAGE_URL = "/health/help/privacy-notice";
    public static final String NOT_ELIGIBLE_IHS_NOT_PAID_PAGE_URL = "/health/eligibility/not-eligible-reimbursement-refund";
    public static final String NOT_ELIGIBLE_TIER5_VISA_PAGE_URL = "/health/eligibility/not-eligible-to-apply";
    public static final String NOT_ELIGIBLE_TIER2_VISA_PAGE_URL = "/health/eligibility/not-eligible-reimbursement";
    public static final String NOT_ELIGIBLE_JOB_RELATED_PAGE_URL = "/health/eligibility/not-eligible";

    public static final String IHS_PAID_PAGE_TITLE = "Has the applicant paid an immigration health surcharge for their current visa? - Apply for your immigration health surcharge reimbursement - GOV.UK";
    public static final String VISA_TYPE_PAGE_TITLE = "What type of visa does the applicant hold? - Apply for your immigration health surcharge reimbursement - GOV.UK";
    public static final String QUALIFIED_HEALTH_PROFESSIONAL_PAGE_TITLE = "Is the applicant a qualified health professional? - Apply for your immigration health surcharge reimbursement - GOV.UK";
    public static final String HEALTH_SOCIAL_CARE_PAGE_TITLE = "Has the applicant had a job in health and social care? - Apply for your immigration health surcharge reimbursement - GOV.UK";
    public static final String WORK_SIX_MONTHS_PAGE_TITLE = "Did the applicant work in the health and social care job for 6 months or more? - Apply for your immigration health surcharge reimbursement - GOV.UK";
    public static final String WORK_AVERAGE_HOURS_PAGE_TITLE = "When working in the health and social care job, was the applicant working an average of 16 hours or more per week? - Apply for your immigration health surcharge reimbursement - GOV.UK";
    public static final String CHECK_ELIGIBILITY_ANSWERS_PAGE_TITLE = "Check your answers for eligibility - Apply for your immigration health surcharge reimbursement - GOV.UK";
    public static final String CLAIM_NAME_PAGE_TITLE = "What's the applicant's name? - Apply for your immigration health surcharge reimbursement - GOV.UK";
    public static final String COOKIES_POLICY_PAGE_TITLE = "Details about cookies for Apply for your immigration health surcharge reimbursement - Apply for your immigration health surcharge reimbursement - GOV.UK";
    public static final String ACCESSIBILITY_STATEMENT_PAGE_TITLE = "Accessibility statement for Apply for your immigration health surcharge reimbursement - Apply for your immigration health surcharge reimbursement - GOV.UK";
    public static final String CONTACT_US_PAGE_TITLE = "Contact us - Apply for your immigration health surcharge reimbursement - GOV.UK";
    public static final String TERMS_CONDITIONS_PAGE_TITLE = "Terms and conditions - Apply for your immigration health surcharge reimbursement - GOV.UK";
    public static final String PRIVACY_NOTICE_PAGE_TITLE = "Privacy notice - Apply for your immigration health surcharge reimbursement - GOV.UK";
    public static final String NOT_ELIGIBLE_IHS_NOT_PAID_PAGE_TITLE = "The applicant is not eligible for an immigration health surcharge reimbursement or a refund - Apply for your immigration health surcharge reimbursement - GOV.UK";
    public static final String NOT_ELIGIBLE_TIER5_VISA_PAGE_TITLE = "The applicant is a Tier 5 Medical Training Initiative (MTI) visa holder - Apply for your immigration health surcharge reimbursement - GOV.UK";
    public static final String NOT_ELIGIBLE_TIER2_VISA_PAGE_TITLE = "The applicant is not eligible for an immigration health surcharge reimbursement but may be entitled to a refund - Apply for your immigration health surcharge reimbursement - GOV.UK";
    public static final String NOT_ELIGIBLE_JOB_RELATED_PAGE_TITLE = "The applicant is not eligible for an immigration health surcharge reimbursement - Apply for your immigration health surcharge reimbursement - GOV.UK";

    public static final String GOV_UK_PAGE = "Welcome to GOV.UK";
    public static final String HELP_PAGE = "Help using GOV.UK - Help Pages - GOV.UK";
    public static final String OPEN_GOVERNMENT_LICENCE_PAGE = "Open Government Licence";
    public static final String CROWN_COPYRIGHT_PAGE = "Crown copyright - Re-using PSI";
    public static final String PAY_FOR_UK_HEALTHCARE_PAGE_TITLE = "Pay for UK healthcare as part of your immigration application: Overview - GOV.UK";
    public static final String PAY_FOR_UK_HEALTHCARE_PAGE_URL = "https://www.gov.uk/healthcare-immigration-application";
    public static final String GOV_UK_TIER2_PAGE_TITLE = "Get an immigration health surcharge refund if you work in health and care: If you have a Tier 2 visa and you’re a medical professional - GOV.UK";
    public static final String GOV_UK_TIER2_PAGE_URL = "https://www.gov.uk/apply-immigration-health-surcharge-refund/tier-2-visa-medical-professional";
    public static final String AOMRC_REFUND_PAGE_TITLE = "IHS Refund Application Form";
    public static final String AOMRC_REFUND_PAGE_URL = "https://form.jotform.com/231763335992060";
    public static final String SNAP_SURVEY_PREVIEW_PAGE = "Survey Preview - Snap Surveys";
    public static final String HEALTH_SNAP_SURVEY_PAGE = "IHS Health and Social Care Digital Feedback";
}