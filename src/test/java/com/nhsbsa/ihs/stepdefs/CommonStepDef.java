package com.nhsbsa.ihs.stepdefs;

import com.nhsbsa.ihs.driver.Config;
import com.nhsbsa.ihs.pageobjects.CommonPage;
import com.nhsbsa.ihs.pageobjects.NotEligiblePage;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.junit.Assert;
import org.openqa.selenium.WebDriver;

import static com.nhsbsa.ihs.stepdefs.Constants.*;

public class CommonStepDef {

    private WebDriver driver;
    private CommonPage commonPage;
    private NotEligiblePage notEligiblePage;

    public CommonStepDef() {
        driver = Config.getDriver();
        commonPage = new CommonPage(driver);
        notEligiblePage = new NotEligiblePage(driver);
    }

    @When("^I select the (.*)$")
    public void iSelectTheHyperlink(String hyperlink) {
        switch (hyperlink) {
            case "Service Name link":
                commonPage.serviceNameLink();
                break;
            case "Back link":
                commonPage.backLink();
                break;
            case "GOV.UK link":
                commonPage.govUKLink();
                break;
            case "Contact Us link":
                notEligiblePage.navigateToContactUs();
                break;
            case "More Info link":
                notEligiblePage.navigateToMoreInfo();
                break;
            case "Tier2 more info link":
                notEligiblePage.navigateToTier2MoreInfo();
                break;
            case "Tier5 Visa Info link":
                notEligiblePage.navigateToTier5VisaInfo();
                break;
            case "Start again":
                notEligiblePage.startAgain();
                break;
            case "Help link":
                commonPage.navigateToHelp();
                break;
            case "Cookies link":
                commonPage.navigateToCookies();
                break;
            case "Accessibility Statement link":
                commonPage.navigateToAccessibilityStatement();
                break;
            case "Contact link":
                commonPage.navigateToContactUs();
                break;
            case "Terms and Conditions link":
                commonPage.navigateToTermsConditions();
                break;
            case "Privacy link":
                commonPage.navigateToPrivacyNotice();
                break;
            case "Open Government Licence link":
                commonPage.navigateToOpenLicence();
                break;
            case "Crown Copyright link":
                commonPage.navigateToCopyrightLogo();
                break;
            case "Accept cookies button":
                commonPage.acceptAnalyticsCookies();
                break;
            case "Reject cookies button":
                commonPage.rejectAnalyticsCookies();
                break;
            case "View cookies link":
                commonPage.navigateToViewCookies();
                break;
            case "Change accept cookie settings link":
                commonPage.acceptAnalyticsCookies();
                commonPage.navigateToAcceptCookieSettings();
                break;
            case "Change reject cookie settings link":
                commonPage.rejectAnalyticsCookies();
                commonPage.navigateToRejectCookieSettings();
                break;
            case "Hide accept message button":
                commonPage.hideAcceptCookieBanner();
                break;
            case "Hide reject message button":
                commonPage.hideRejectCookieBanner();
                break;
            case "Feedback link":
                commonPage.navigateToSurveyPreview();
                break;
        }
    }

    @When("^I see the cookies banner is displayed$")
    public void iSeeTheCookiesBannerIsDisplayed() {
        Assert.assertTrue(commonPage.isCookieBannerDisplayed());
    }

    @Then("^I see the cookies banner is hidden$")
    public void iSeeTheCookiesBannerIsHidden() {
        Assert.assertFalse(commonPage.isCookieBannerDisplayed());
    }

    @And("^I start the survey$")
    public void iStartTheSurvey() {
        commonPage.navigateToSnapSurvey();
    }
}
