package com.nhsbsa.ihs.stepdefs;

import com.nhsbsa.ihs.driver.Config;
import com.nhsbsa.ihs.pageobjects.*;
import com.nhsbsa.ihs.utilities.ConfigReader;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.When;
import org.openqa.selenium.WebDriver;

public class BackgroundStepDef {

    private WebDriver driver;
    private String baseUrl;
    private String testGovUrl;
    private CommonPage commonPage;
    private com.nhsbsa.ihs.pageobjects.IHSPaidPage IHSPaidPage;
    private VisaTypePage visaTypePage;
    private HealthSocialCarePage healthSocialCarePage;
    private WorkSixMonthsPage workSixMonthsPage;

    public BackgroundStepDef() {
        driver = Config.getDriver();
        baseUrl = ConfigReader.getEnvironment();
        testGovUrl = ConfigReader.getGovTestURL();
        commonPage = new CommonPage(driver);
        IHSPaidPage = new IHSPaidPage(driver);
        visaTypePage = new VisaTypePage(driver);
        healthSocialCarePage = new HealthSocialCarePage(driver);
        workSixMonthsPage = new WorkSixMonthsPage(driver);
    }

    @Given("^I launch the IHS Health eligibility application$")
    public void iLaunchTheIHSHealthEligibilityApplication() {
        driver.manage().deleteAllCookies();
        new Page(driver).navigateToUrl(baseUrl);
    }

    @Given("^I launch the IHS Health Gov Test link$")
    public void iLaunchTheIHSHealthGovTestLink() {
        driver.manage().deleteAllCookies();
        new Page(driver).navigateToUrl(testGovUrl);
    }

    @When("^I start the service$")
    public void iStartTheService() {
        commonPage.clickStartLink();
    }

    @And("^I have paid the immigration health surcharge$")
    public void iHavePaidTheImmigrationHealthSurcharge() {
        IHSPaidPage.selectYesRadioButtonAndClickContinue();
    }

    @And("^I hold a Tier2Visa$")
    public void iHoldATierVisa() {
        IHSPaidPage.selectYesRadioButtonAndClickContinue();
        visaTypePage.selectTier2AndContinue();
    }

    @And("^I do not hold a Tier2Visa or Tier5Visa$")
    public void iDoNotHoldATierVisaOrTierVisa() {
        IHSPaidPage.selectYesRadioButtonAndClickContinue();
        visaTypePage.selectOtherVisaAndContinue();
    }

    @And("^I have worked in health and social care job$")
    public void iHaveWorkedInHealthAndSocialCareJob() {
        IHSPaidPage.selectYesRadioButtonAndClickContinue();
        visaTypePage.selectOtherVisaAndContinue();
        healthSocialCarePage.selectYesRadioButtonAndClickContinue();
    }

    @And("^I have worked for six months in health and social care$")
    public void iHaveWorkedForSixMonthsInHealthAndSocialCare() {
        IHSPaidPage.selectYesRadioButtonAndClickContinue();
        visaTypePage.selectOtherVisaAndContinue();
        healthSocialCarePage.selectYesRadioButtonAndClickContinue();
        workSixMonthsPage.selectYesRadioButtonAndClickContinue();
    }
}
