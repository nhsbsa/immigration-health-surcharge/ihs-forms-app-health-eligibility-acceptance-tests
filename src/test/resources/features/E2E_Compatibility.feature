@Compatibility

Feature: Validation of the IHS Health Eligibility end-to-end functionality in various SauceLabs devices to meet the compatibility test criteria

  Background:
    Given I launch the IHS Health eligibility application

  Scenario Outline: Validate the end-to-end functionality in the Eligibility app for Tier2 visa holder
    When I <ihsOption> paid the Immigration Health Surcharge
    And I hold <visaType> visa
    And I <qhpOption> a qualified health professional
    And I <hscOption> worked in health and social care
    And I <wsmOption> worked for six months or more
    And I <ashOption> worked for an average of sixteen hours
    Then I will see <answers> eligibility answers
    When I submit the eligibility answers
    Then I will see the <output>

    Examples:
      | ihsOption | visaType | qhpOption | hscOption | wsmOption | ashOption | answers                                | output            |
      | have      | Tier2    | am not    | have      | have      | have      | 1=Yes 2=Yes 3=No 4=Yes 5=Yes and 6=Yes | Claim Name screen |

  Scenario Outline: Validate the end-to-end functionality in the Eligibility app for Other visa holder
    When I <ihsOption> paid the Immigration Health Surcharge
    And I hold <visaType> visa
    And I <hscOption> worked in health and social care
    And I <wsmOption> worked for six months or more
    And I <ashOption> worked for an average of sixteen hours
    Then I will see <answers> eligibility answers
    When I submit the eligibility answers
    Then I will see the <output>

    Examples:
      | ihsOption | visaType | hscOption | wsmOption | ashOption | answers                           | output            |
      | have      | Other    | have      | have      | have      | 1=Yes 2=Yes 3=Yes 4=Yes and 5=Yes | Claim Name screen |