@HealthSocialCare @IHSRI-1569 @Regression

Feature: Validation of Health and Social Care page to verify the applicant's eligibility for Immigration Health Surcharge Reimbursement

  Background:
    Given I launch the IHS Health eligibility application
    And I do not hold a Tier2Visa or Tier5Visa

  @Smoke @Retest-1656
  Scenario Outline: Validate the various options an applicant can select on Health Social Care page
    When I <hscOption> worked in health and social care
    Then I will see the <output>
    Examples:
      | hscOption | output                                |
      | have      | HSC 6 Months screen                   |
      | have not  | Not Eligible for Reimbursement screen |
      |           | Health Social Care Error Message      |

  @Smoke @Retest-1661
  Scenario Outline: Validate the hyperlinks on Health Social Care page
    When I select the <hyperlink>
    Then I will see the <screen>
    Examples:
      | hyperlink         | screen                    |
      | Back link         | Visa Type Question screen |
      | Service Name link | Start screen              |
      | GOV.UK link       | GOV UK screen             |

  @Retest-1591 @Retest-1657
  Scenario Outline: Validate the hyperlinks on Not Eligible for Reimbursement page
    When I <hscOption> worked in health and social care
    And I select the <hyperlink>
    Then I will see the <screen>
    Examples:
      | hscOption | hyperlink         | screen            |
      | have not  | Service Name link | Start screen      |
      | have not  | GOV.UK link       | GOV UK screen     |
      | have not  | Contact Us link   | Contact Us screen |
      | have not  | Start again       | Start screen      |