@CheckEligibilityAnswers @IHSRI-1592 @Regression

Feature: Validation of Check Your Answers page on Eligibility web app to verify the applicant's eligibility for reimbursement

  Background:
    Given I launch the IHS Health eligibility application

  @Smoke
  Scenario Outline: Validate the eligibility answers for 'Tier2' visa holder on Check Eligibility Answers page
    When I <ihsOption> paid the Immigration Health Surcharge
    And I hold <visaType> visa
    And I <qhpOption> a qualified health professional
    And I <hscOption> worked in health and social care
    And I <wsmOption> worked for six months or more
    And I <ashOption> worked for an average of sixteen hours
    Then I will see <answers> eligibility answers

    Examples:
      | ihsOption | visaType | qhpOption | hscOption | wsmOption | ashOption | answers                                |
      | have      | Tier2    | am not    | have      | have      | have      | 1=Yes 2=Yes 3=No 4=Yes 5=Yes and 6=Yes |

  Scenario Outline: Validate the eligibility answers for 'Other' visa holder on Check Eligibility Answers page
    When I <ihsOption> paid the Immigration Health Surcharge
    And I hold <visaType> visa
    And I <hscOption> worked in health and social care
    And I <wsmOption> worked for six months or more
    And I <ashOption> worked for an average of sixteen hours
    Then I will see <answers> eligibility answers

    Examples:
      | ihsOption | visaType | hscOption | wsmOption | ashOption | answers                           |
      | have      | Other    | have      | have      | have      | 1=Yes 2=Yes 3=Yes 4=Yes and 5=Yes |

  @Retest-1660
  Scenario Outline: Validate the change links on Check Eligibility Answers page without changing the answer
    When I hold <visaType> visa and eligible for reimbursement
    And I select <hyperlink> on Check Eligibility Answers page
    Then I will see the <screen>
    And I will see <answer> selected
    When I continue without changing my answer
    Then I will see the Check Eligibility Answers screen
    And My answer remains the same as <answer>

    Examples:
      | visaType | hyperlink                       | screen                     | answer                 |
      | Tier2    | Change IHS Paid link            | IHS Paid screen            | Paid IHS Yes           |
      | Tier2    | Change Visa Type link           | Visa Type Question screen  | Tier2 Visa Yes         |
      | Other    | Change Visa Type link           | Visa Type Question screen  | Other Visa Yes         |
      | Tier2    | Change Health Professional link | Health Professional screen | Health Professional No |
      | Other    | Change Health Social Care link  | Health Social Care screen  | Health Social Care Yes |
      | Tier2    | Change HSC 6 Months link        | HSC 6 Months screen        | Work 6 Months Yes      |
      | Other    | Change Average 16 Hours link    | Average 16 Hours screen    | Average 16 Hours Yes   |

  @Smoke @Retest-1654 @Retest-1656 @Retest-1658 @Retest-1659
  Scenario Outline: Validate the change links on Check Eligibility Answers page on changing the answer
    When I hold <visaType> visa and eligible for reimbursement
    And I select <hyperlink> on Check Eligibility Answers page
    Then I will see the <screen>
    When I change my answer as <radioButton>
    Then I will see the <newScreen>

    Examples:
      | visaType | hyperlink                       | screen                     | radioButton             | newScreen                                        |
      | Other    | Change IHS Paid link            | IHS Paid screen            | Paid IHS No             | Not Eligible for Reimbursement or Refund screen  |
      | Tier2    | Change Visa Type link           | Visa Type Question screen  | Tier5 Visa Yes          | Tier5 Not Eligible to Apply screen               |
      | Other    | Change Visa Type link           | Visa Type Question screen  | Tier5 Visa Yes          | Tier5 Not Eligible to Apply screen               |
      | Tier2    | Change Health Professional link | Health Professional screen | Health Professional Yes | Not Eligible for Reimbursement but Refund screen |
      | Tier2    | Change Health Social Care link  | Health Social Care screen  | Health Social Care No   | Not Eligible for Reimbursement screen            |
      | Other    | Change HSC 6 Months link        | HSC 6 Months screen        | Work 6 Months No        | Not Eligible for Reimbursement screen            |
      | Tier2    | Change Average 16 Hours link    | Average 16 Hours screen    | Average 16 Hours No     | Not Eligible for Reimbursement screen            |

  @Smoke @Retest-1660
  Scenario Outline: Validate the change links when 'Tier2' visa is changed to 'Other' visa
    When I hold <visaType> visa and eligible for reimbursement
    And I select <hyperlink> on Check Eligibility Answers page
    Then I will see the <screen>
    When I change my answer as <radioButton>
    Then I will see the Check Eligibility Answers screen
    And I will see <answers> eligibility answers

    Examples:
      | visaType | hyperlink             | screen                    | radioButton    | answers                           |
      | Tier2    | Change Visa Type link | Visa Type Question screen | Other Visa Yes | 1=Yes 2=Yes 3=Yes 4=Yes and 5=Yes |

  @Smoke @Retest-1662
  Scenario Outline: Validate the change links when 'Other' visa is changed to 'Tier2' visa and eligible
    When I hold <visaType> visa and eligible for reimbursement
    And I select <hyperlink> on Check Eligibility Answers page
    Then I will see the <screen>
    When I change my answer as <radioButton>
    Then I will see the Health Professional screen
    When I <qhpOption> a qualified health professional
    Then I will see the Check Eligibility Answers screen
    And I will see <answers> eligibility answers

    Examples:
      | visaType | hyperlink             | screen                    | radioButton    | qhpOption | answers                                |
      | Other    | Change Visa Type link | Visa Type Question screen | Tier2 Visa Yes | am not    | 1=Yes 2=Yes 3=No 4=Yes 5=Yes and 6=Yes |

  @Retest-1654
  Scenario Outline: Validate the change links when 'Other' visa is changed to 'Tier2' visa and not eligible
    When I hold <visaType> visa and eligible for reimbursement
    And I select <hyperlink> on Check Eligibility Answers page
    Then I will see the <screen>
    When I change my answer as <radioButton>
    Then I will see the Health Professional screen
    When I <qhpOption> a qualified health professional
    Then I will see the Not Eligible for Reimbursement but Refund screen

    Examples:
      | visaType | hyperlink             | screen                    | radioButton    | qhpOption |
      | Other    | Change Visa Type link | Visa Type Question screen | Tier2 Visa Yes | am        |

  @Retest-1664
  Scenario Outline: Validate the navigation to the Check Eligibility Answers Page from change links
    When I hold Tier2 visa and eligible for reimbursement
    And I select <hyperlink> on Check Eligibility Answers page
    Then I will see the <screen>
    When I select the Back link
    And I continue without changing my answer
    Then I will see the Check Eligibility Answers screen
    And My answer remains the same as <answer>

    Examples:
      | hyperlink                       | screen                     | answer                 |
      | Change Visa Type link           | Visa Type Question screen  | Tier2 Visa Yes         |
      | Change Health Professional link | Health Professional screen | Health Professional No |
      | Change Health Social Care link  | Health Social Care screen  | Health Social Care Yes |
      | Change HSC 6 Months link        | HSC 6 Months screen        | Work 6 Months Yes      |
      | Change Average 16 Hours link    | Average 16 Hours screen    | Average 16 Hours Yes   |

  Scenario Outline: Validate the hyperlinks on Check Eligibility Answers page
    When I hold Tier2 visa and eligible for reimbursement
    And I select <hyperlink> on Check Eligibility Answers page
    Then I will see the <screen>

    Examples:
      | hyperlink         | screen                  |
      | Service Name link | Start screen            |
      | GOV.UK link       | GOV UK screen           |

  @Smoke @EndToEnd @Retest-1770
  Scenario Outline: Validate the successful completion of check eligibility answers
    When I hold Tier2 visa and eligible for reimbursement
    And I have verified my eligibility answers
    Then I will see the <screen>

    Examples:
      | screen      |
      | Name screen |