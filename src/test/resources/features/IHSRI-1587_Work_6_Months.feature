@HSC6Months @IHSRI-1587 @Regression

Feature: Validation of Worked Six Months page to verify the applicant's eligibility for Immigration Health Surcharge Reimbursement

  Background:
    Given I launch the IHS Health eligibility application
    And I have worked in health and social care job

  @Smoke @Retest-1658
  Scenario Outline: Validate the various options an applicant can select on HSC 6 Months page
    When I <wsmOption> worked for six months or more
    Then I will see the <output>
    Examples:
      | wsmOption | output                                |
      | have      | Average 16 Hours screen               |
      | have not  | Not Eligible for Reimbursement screen |
      |           | HSC 6 Months Error Message screen     |

  Scenario Outline: Validate the hyperlinks on HSC 6 Months page
    When I select the <hyperlink>
    Then I will see the <screen>
    Examples:
      | hyperlink         | screen                    |
      | Back link         | Health Social Care screen |
      | Service Name link | Start screen              |
      | GOV.UK link       | GOV UK screen             |

  @Smoke @Retest-1591 @Retest-1657
  Scenario Outline: Validate the hyperlinks on Not Eligible for Reimbursement page
    When I <wsmOption> worked for six months or more
    And I select the <hyperlink>
    Then I will see the <screen>
    Examples:
      | wsmOption | hyperlink         | screen            |
      | have not  | Service Name link | Start screen      |
      | have not  | GOV.UK link       | GOV UK screen     |
      | have not  | Contact Us link   | Contact Us screen |
      | have not  | Start again       | Start screen      |