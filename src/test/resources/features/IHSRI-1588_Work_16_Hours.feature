@Average16Hours @IHSRI-1588 @Regression

Feature: Validation of Average Sixteen Hours page to verify the applicant's eligibility for Immigration Health Surcharge Reimbursement

  Background:
    Given I launch the IHS Health eligibility application
    And I have worked for six months in health and social care

  @Smoke @Retest-1569 @Retest-1660
  Scenario Outline: Validate the various options an applicant can select on HSC Average 16 Hours page
    When I <ashOption> worked for an average of sixteen hours
    Then I will see the <output>
    Examples:
      | ashOption | output                                |
      | have      | Check Eligibility Answers screen      |
      | have not  | Not Eligible for Reimbursement screen |
      |           | Average 16 Hours Error Message        |

  Scenario Outline: Validate the hyperlinks on HSC Average 16 Hours page
    When I select the <hyperlink>
    Then I will see the <screen>
    Examples:
      | hyperlink         | screen              |
      | Back link         | HSC 6 Months screen |
      | Service Name link | Start screen        |
      | GOV.UK link       | GOV UK screen       |

  @Retest-1591 @Retest-1657
  Scenario Outline: Validate the hyperlinks on Not Eligible for Reimbursement page
    When I <ashOption> worked for an average of sixteen hours
    And I select the <hyperlink>
    Then I will see the <screen>
    Examples:
      | ashOption | hyperlink         | screen            |
      | have not  | Service Name link | Start screen      |
      | have not  | GOV.UK link       | GOV UK screen     |
      | have not  | Contact Us link   | Contact Us screen |
      | have not  | Start again       | Start screen      |