@HealthProfessional @IHSRI-1568 @Regression

Feature: Validation of Qualified Health Professional page to verify the applicant's eligibility for reimbursement/refund

  Background:
    Given I launch the IHS Health eligibility application
    And I hold a Tier2Visa

  @Smoke @Retest-1654
  Scenario Outline: Validate the various options an applicant can select on Qualified Health Professional page
    When I <qhpOption> a qualified health professional
    Then I will see the <output>
    Examples:
      | qhpOption | output                                           |
      | am        | Not Eligible for Reimbursement but Refund screen |
      | am not    | Health Social Care screen                        |
      |           | Health Professional Error Message                |

  Scenario Outline: Validate the hyperlinks on Qualified Health Professional page
    When I select the <hyperlink>
    Then I will see the <screen>
    Examples:
      | hyperlink         | screen                    |
      | Back link         | Visa Type Question screen |
      | Service Name link | Start screen              |
      | GOV.UK link       | GOV UK screen             |

  @Smoke @Retest-1655
  Scenario Outline: Validate the hyperlinks on Tier2 Not Eligible for Reimbursement page
    When I <qhpOption> a qualified health professional
    And I select the <hyperlink>
    Then I will see the <screen>
    Examples:
      | qhpOption | hyperlink            | screen              |
      | am        | Service Name link    | GOV UK Start screen |
      | am        | GOV.UK link          | GOV UK screen       |
      | am        | Tier2 more info link | GOV UK Tier2 screen |