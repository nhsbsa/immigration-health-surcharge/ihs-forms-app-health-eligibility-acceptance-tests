@StartStub @IHSRI-1715 @NeedJavaScript @Regression

Feature: Validation of the integration between Start Stub and IHS Health reimbursement service

  Background:
    Given I launch the IHS Health Gov Test link

  Scenario Outline: Validate the cookies banner is visible when redirected from Start Stub to IHS Health eligibility
    When I start the service
    Then I will see the <screen>
    And I see the cookies banner is displayed
    Examples:
      | screen          |
      | IHS Paid screen |

  Scenario Outline: Validate the functionality of the cookies banner when redirected from Start Stub to IHS Health eligibility
    When I start the service
    And I see the cookies banner is displayed
    And I select the <button1>
    Then I will see the <message>
    When I select the <button2>
    Then I see the cookies banner is hidden

    Examples:
      | button1               | message                       | button2                    |
      | Accept cookies button | Cookies accepted confirmation | Hide accept message button |
      | Reject cookies button | Cookies rejected confirmation | Hide reject message button |

  @IHSRI-2865
  Scenario Outline: Validate the in-service feedback survey when redirected from Start Stub to IHS Health eligibility
    When I start the service
    And I select the <hyperlink>
    And I will see the Snap survey preview
    And I start the survey
    Then I will see the <output>
    Examples:
      | hyperlink     | output                           |
      | Feedback link | IHS Health Service Survey screen |

  Scenario Outline: Validate the footer links when redirected from Start Stub to IHS Health eligibility
    When I start the service
    And I select the <hyperlink>
    Then I will see the <screen>
    Examples:
      | hyperlink                    | screen                         |
      | Help link                    | Help screen                    |
      | Cookies link                 | Cookies Policy screen          |
      | Accessibility Statement link | Accessibility Statement screen |
      | Contact link                 | Contact Us screen              |
      | Terms and Conditions link    | Terms and Conditions screen    |
      | Privacy link                 | Privacy Notice screen          |
      | Open Government Licence link | Open Government Licence screen |
      | Crown Copyright link         | Crown Copyright screen         |

  Scenario Outline: Validate the integration with claim app when redirected from Start Stub to IHS Health eligibility
    When I start the service
    And I hold Tier2 visa and eligible for reimbursement
    And I have verified my eligibility answers
    Then I will see the <screen>
    Examples:
      | screen      |
      | Name screen |