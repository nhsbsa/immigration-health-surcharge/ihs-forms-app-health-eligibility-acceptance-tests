@IHSPaid @IHSRI-1566 @Regression

Feature: Validation of IHS Paid page to verify the applicant's eligibility for Immigration Health Surcharge Reimbursement

  Background:
    Given I launch the IHS Health eligibility application

  @Smoke
  Scenario Outline: Validate the various options an applicant can select on IHS Payment Question page
    When I <ihsOption> paid the Immigration Health Surcharge
    Then I will see the <output>
    Examples:
      | ihsOption | output                                          |
      | have      | Visa Type Question screen                       |
      | have not  | Not Eligible for Reimbursement or Refund screen |
      |           | IHS Payment Error Message                       |

  @Retest-1709 @Retest-1712
  Scenario Outline: Validate the hyperlinks on IHS Payment page
    When I select the <hyperlink>
    Then I will see the <screen>
    Examples:
      | hyperlink         | screen        |
      | Service Name link | Start screen  |
      | GOV.UK link       | GOV UK screen |

  @Smoke @Retest-1589 @Retest-1653
  Scenario Outline: Validate the hyperlinks on Not Eligible for Reimbursement or Refund page
    When I <ihsOption> paid the Immigration Health Surcharge
    And I select the <hyperlink>
    Then I will see the <screen>
    Examples:
      | ihsOption | hyperlink         | screen                       |
      | have not  | Service Name link | Start screen                 |
      | have not  | GOV.UK link       | GOV UK screen                |
      | have not  | Contact Us link   | Contact Us screen            |
      | have not  | More Info link    | Pay for UK healthcare screen |