@VisaType @IHSRI-1567 @Regression

Feature: Validation of Visa Type Questionnaire page to verify the applicant's eligibility for reimbursement/refund

  Background:
    Given I launch the IHS Health eligibility application
    And I have paid the immigration health surcharge

  @Smoke
  Scenario Outline: Validate the various options an applicant can select on Visa Type Question page
    When I hold <visaType> visa
    Then I will see the <output>
    Examples:
      | visaType | output                             |
      | Tier2    | Health Professional screen         |
      | Tier5    | Tier5 Not Eligible to Apply screen |
      | Other    | Health Social Care screen          |
      |          | Visa Type Error Message            |

  Scenario Outline: Validate the hyperlinks on Visa Type Question page
    When I select the <hyperlink>
    Then I will see the <screen>
    Examples:
      | hyperlink         | screen          |
      | Back link         | IHS Paid screen |
      | Service Name link | Start screen    |
      | GOV.UK link       | GOV UK screen   |

  @Smoke @Retest-1590 @Retest-1653 @Retest-1669 @IHSRI-2813 @Retest-3122
  Scenario Outline: Validate the hyperlinks on Tier5 Not Eligible page
    When I hold <visaType> visa
    And I select the <hyperlink>
    Then I will see the <screen>
    Examples:
      | visaType | hyperlink            | screen                        |
      | Tier5    | Service Name link    | Start screen                  |
      | Tier5    | GOV.UK link          | GOV UK screen                 |
      | Tier5    | Tier5 Visa Info link | AOMRC surcharge refund screen |
      | Tier5    | More Info link       | Pay for UK healthcare screen  |