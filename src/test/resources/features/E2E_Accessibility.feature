@Accessibility

Feature: Validation of the accessibility of IHS Health Eligibility app using Axe, Lighthouse and Wave tool

  Scenario Outline: Validate the end-to-end accessibility of IHS Health Eligibility app
    Given I launch the IHS Health eligibility application on <browser>
    When I have paid the Immigration Health Surcharge
    And I hold Tier2 visa
    And I am not a qualified health professional
    And I have worked in health and social care
    And I have worked for six months or more
    And I have worked for an average of sixteen hours
    When I submit the eligibility answers

    Examples:
    | browser |
    | chrome  |
    | firefox |

  Scenario Outline: Validate the accessibility for Not Eligible Reimbursement Refund screen
    Given I launch the IHS Health eligibility application on <browser>
    When I have not paid the Immigration Health Surcharge
    Examples:
      | browser |
      | chrome  |
      | firefox |

  Scenario Outline: Validate the accessibility for Not Eligible to Apply screen
    Given I launch the IHS Health eligibility application on <browser>
    When I have paid the Immigration Health Surcharge
    And I hold Tier5 visa
    Examples:
      | browser |
      | chrome  |
      | firefox |

  Scenario Outline: Validate the accessibility for Not Eligible Reimbursement screen
    Given I launch the IHS Health eligibility application on <browser>
    When I hold a Tier2Visa
    And I am a qualified health professional
    Examples:
      | browser |
      | chrome  |
      | firefox |

  Scenario Outline: Validate the accessibility for Not Eligible screen
    Given I launch the IHS Health eligibility application on <browser>
    When I have worked for six months in health and social care
    And I have not worked for an average of sixteen hours
    Examples:
      | browser |
      | chrome  |
      | firefox |