package com.nhsbsa.ihs.shared;

public class SharedData {

    private SharedData() {
        throw new IllegalStateException("Shared Data Utility class");
    }

    public static String environment;
    public static String browser;

}
