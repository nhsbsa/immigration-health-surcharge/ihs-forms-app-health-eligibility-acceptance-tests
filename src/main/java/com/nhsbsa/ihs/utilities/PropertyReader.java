package com.nhsbsa.ihs.utilities;

import com.nhsbsa.ihs.shared.SharedData;
import org.apache.commons.lang3.StringUtils;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class PropertyReader {

    private PropertyReader() {
        throw new IllegalStateException("Preperty Reader Utility CLass");
    }

    public static String getProperty(String key) throws IOException {
        SharedData.environment = System.getProperty("env");
        if(StringUtils.isEmpty(SharedData.environment)) {
            return null;
        }

        final String propertiesFilePath = "/" + SharedData.environment.toLowerCase() + ".properties";
        try(InputStream inputStream = PropertyReader.class.getResourceAsStream(propertiesFilePath)) {
            Properties properties = new Properties();
            properties.load(inputStream);
            return properties.getProperty(key);
        }
    }
}

