package com.nhsbsa.ihs.pageobjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class HealthSocialCarePage extends Page {

    private By yesRadioButtonLocator = By.id("health-social-care-job-yes");
    private By noRadioButtonLocator = By.id("health-social-care-job-no");
    private By continueButtonLocator = By.id("continue-button");
    private By errorMessageLocator = By.partialLinkText("Select 'Yes' if");

    public HealthSocialCarePage(WebDriver driver) {
        super(driver);
    }

    public void selectYesRadioButtonAndClickContinue() {
        clickEvent(yesRadioButtonLocator);
        continueButton();
    }

    public void selectNoRadioButtonAndClickContinue() {
        clickEvent(noRadioButtonLocator);
        continueButton();
    }

    public void continueButton() {
        clickEvent(continueButtonLocator);
    }

    public String getErrorMessage() {
        return getElementText(errorMessageLocator);
    }

    public boolean isYesRadioButtonSelected() {
        return getElementIsSelected(yesRadioButtonLocator);
    }
}

