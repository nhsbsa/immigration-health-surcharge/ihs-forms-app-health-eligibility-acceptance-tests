package com.nhsbsa.ihs.pageobjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class NotEligiblePage extends Page {

    private By contactUsLocator = By.id("contact-us-page-link");
    private By moreInfoLocator = By.id("more-info-link");
    private By tier2InfoLocator = By.id("tier-2-info");
    private By tier5VisaInfoLocator = By.id("refund-immigration-link");
    private By startAgainLocator = By.id("start-page-link");

    public NotEligiblePage(WebDriver driver) {
        super(driver);
    }

    public void navigateToContactUs () {
        clickEvent(contactUsLocator);
    }

    public void navigateToMoreInfo () {
        clickEvent(moreInfoLocator);
    }

    public void navigateToTier2MoreInfo () {
        clickEvent(tier2InfoLocator);
    }

    public void navigateToTier5VisaInfo () {
        clickEvent(tier5VisaInfoLocator);
    }

    public void startAgain () {
        clickEvent(startAgainLocator);
    }
}
