package com.nhsbsa.ihs.pageobjects;

import org.openqa.selenium.*;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.openqa.selenium.By;

import java.time.Duration;
import java.util.NoSuchElementException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Page {

    protected WebDriver driver;
    private WebElement element;
    public int explicitWaitTime = 50;
    public WebDriverWait webDriverWait;

    public Page(WebDriver driver) {
        this.driver = driver;
    }

    public void navigateToUrl(String url) {
        driver.get(url);
    }

    public WebDriverWait getWebDriverWait() {
        webDriverWait = new WebDriverWait(driver, Duration.ofSeconds(explicitWaitTime));
        webDriverWait.ignoring(NoSuchElementException.class);
        webDriverWait.ignoring(StaleElementReferenceException.class);
        webDriverWait.ignoring(ElementNotInteractableException.class);
        return webDriverWait;
    }

    public void waitForPageLoad() {
        Logger logger = Logger.getLogger(Page.class.getName());
        Wait<WebDriver> wait = new WebDriverWait(driver, Duration.ofSeconds(explicitWaitTime));
        wait.until(d -> {
            logger.log(Level.INFO,"Current Window State: {0}",String.valueOf(((JavascriptExecutor) driver).executeScript("return document.readyState")));
            return String
                    .valueOf(((JavascriptExecutor) driver).executeScript("return document.readyState"))
                    .equals("complete");
        });
    }

    public void clickEvent(By by) {
        webDriverWait = getWebDriverWait();
        WebElement ele = webDriverWait.until(ExpectedConditions.presenceOfElementLocated(by));
        ele.click();
    }

    public String getElementText(By by) {
        return driver.findElement(by).getText();
    }

    public boolean getElementIsDisplayed(By by) {
        return driver.findElement(by).isDisplayed();
    }

    public boolean getElementIsSelected(By by) {
        return driver.findElement(by).isSelected();
    }

    public String getPageTitles(){
        return driver.getTitle();
    }

    public void click() {
        element.click();
    }

    public void tearDownDriver() {
        driver.quit();
    }

    public String getUrl() {
        return driver.getCurrentUrl();
    }
}
