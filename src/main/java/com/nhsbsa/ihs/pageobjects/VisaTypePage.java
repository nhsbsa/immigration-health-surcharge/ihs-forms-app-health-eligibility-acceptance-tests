package com.nhsbsa.ihs.pageobjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class VisaTypePage extends Page{

    private By tier2VisaTypeLocator = By.id("applicant-visa-type-tier2");
    private By tier5VisaTypeLocator = By.id("applicant-visa-type-tier5");
    private By otherVisaTypeLocator = By.id("applicant-visa-type-other");
    private By continueButtonLocator = By.id("continue-button");
    private By errorMessageLocator = By.partialLinkText("Select the type");

    public VisaTypePage(WebDriver driver) {
        super(driver);
    }

    public void selectTier2AndContinue() {
        clickEvent(tier2VisaTypeLocator);
        continueButton();
    }

    public void selectTier5AndContinue() {
        clickEvent(tier5VisaTypeLocator);
        continueButton();
    }

    public void selectOtherVisaAndContinue() {
        clickEvent(otherVisaTypeLocator);
        continueButton();
    }

    public void continueButton() {
        clickEvent(continueButtonLocator);
    }

    public String getErrorMessage() {
        return getElementText(errorMessageLocator);
    }

    public boolean isTier2VisaSelected() {
        return getElementIsSelected(tier2VisaTypeLocator);
    }

    public boolean isOtherVisaSelected() {
        return getElementIsSelected(otherVisaTypeLocator);
    }
}
