package com.nhsbsa.ihs.pageobjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class CheckEligibilityAnswersPage extends Page {

    private By checkAndAcceptButtonLocator = By.id("health-eligibility-app-link");
    private By changeLinkIHSPaymentLocator = By.id("/immigration-health-surcharge-paid-change-link");
    private By changeLinkTier2QuestionLocator = By.id("/applicant-visa-type-change-link");
    private By changeLinkQualifiedHealthProfessionalLocator = By.id("/qualified-health-professional-change-link");
    private By changeLinkHSCWorkLocator = By.id("/health-social-care-job-change-link");
    private By changeLinkSixMonthsLocator = By.id("/job-6-months-change-link");
    private By changeLinkAverageTimeLocator = By.id("/job-average-16-hours-change-link");
    private By ihsAnswer = By.id("/immigration-health-surcharge-paid-value");
    private By visaAnswer = By.id("/applicant-visa-type-value");
    private By qhpAnswer = By.id("/qualified-health-professional-value");
    private By hscAnswer = By.id("/health-social-care-job-value");
    private By wsmAnswer = By.id("/job-6-months-value");
    private By ashAnswer = By.id("/job-average-16-hours-value");

    public CheckEligibilityAnswersPage(WebDriver driver) {
        super(driver);
    }

    public void checkAndAcceptButton() {
        clickEvent(checkAndAcceptButtonLocator);
    }

    public void changeLinkIHSPayment() {
        clickEvent(changeLinkIHSPaymentLocator);
    }
    public void changeLinkHSCWork() {
        clickEvent(changeLinkHSCWorkLocator);
    }
    public void changeLinkSixMonths() {
        clickEvent(changeLinkSixMonthsLocator);
    }
    public void changeLinkAverageTime() {
        clickEvent(changeLinkAverageTimeLocator);
    }
    public void changeLinkTier2Question() {
        clickEvent(changeLinkTier2QuestionLocator);
    }
    public void changeLinkQualifiedHealthProfessional() {
        clickEvent(changeLinkQualifiedHealthProfessionalLocator);
    }

    public String getIHSPaidAnswer() {
        return getElementText(ihsAnswer);
    }
    public String getVisaTypeAnswer() {
        return getElementText(visaAnswer);
    }
    public String getQualifiedHealthProfessionalAnswer() {
        return getElementText(qhpAnswer);
    }
    public String getHealthSocialCareAnswer() {
        return getElementText(hscAnswer);
    }
    public String getWorkSixMonthsAnswer() {
        return getElementText(wsmAnswer);
    }
    public String getAverageSixteenHoursAnswer() {
        return getElementText(ashAnswer);
    }

}
