package com.nhsbsa.ihs.pageobjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class StartPage extends Page {

    private By startNowButtonLocator = By.xpath("//a[@role='button']");

    public StartPage(WebDriver driver) {
        super(driver);
    }

    public void startNow() {
        clickEvent(startNowButtonLocator);
    }
}


