# Contributing to Immigration Health Surcharge Forms

ihs-forms-app-health-eligibility-acceptance-tests is released under the [Apache 2 license](LICENSE). If you would like to contribute
something, or simply want to hack on the code this document should help you get started.

## Code of Conduct

This project adheres to the Contributor Covenant [code of conduct](CODE_OF_CONDUCT.md).
By participating, you are expected to uphold this code.

Please report unacceptable behavior to <nhsbsa.opensource@nhs.net>

## Using Gitlab Issues

We use Gitlab issues to track community reported bugs and enhancements.

If you are reporting a bug, please help to speed up problem diagnosis by providing as
much information as possible. Ideally, that would include a small sample project that
reproduces the problem.

## A quick guide on how to contribute

1. Fork the project

2. Clone the repo from your own space

3. Use [README.md](/README.md) instructions to build/run.

4. Create a branch

5. Add your functionality or bug fix and a test for your change. Only refactoring and
   documentation changes do not require tests.

6. Make sure all the tests pass.

7. Push to your fork, and submit a merge request.

## Cloning the git repository on Windows

Some files in the git repository may exceed the Windows maximum file path (260
characters), depending on where you clone the repository. If you get `Filename too long`
errors, set the `core.longPaths=true` git option:

```bash
git clone -c core.longPaths=true {project URL}
```

## Comments on this policy

If you have suggestions on how this policy could be improved please submit a pull request.
