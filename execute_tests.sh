#!/bin/sh
export ENVIRONMENT=test

#####run the functional tests#####
export BROWSER=headless
#export BROWSER=chrome

export RUNNER=RegressionTestRunner
#export RUNNER=SmokeTestRunner
#export RUNNER=GenericTestRunner

mvn clean verify -Dbrowser=${BROWSER} -Denv=${ENVIRONMENT} -Dit.test=${RUNNER}